// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TestingGroundsGameMode.h"
#include "InfiniteTerrainGameMode.generated.h"

/**
 * 
 */
class ANavMeshBoundsVolume;
class UActorPool;

UCLASS()
class TESTINGGROUNDS_API AInfiniteTerrainGameMode : public ATestingGroundsGameMode
{
	GENERATED_BODY()
	
public:
	AInfiniteTerrainGameMode();

	UFUNCTION(BlueprintCallable, Category = "Bounds Pool")
	void PopulateBoundsVolumePool();

	UFUNCTION(BlueprintCallable, Category = "Score")
	void NewTileConquered();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FORCEINLINE	int32 GetScore() { return Score; }
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pool")
	UActorPool* NaveMeshVolumePool;


private:
	void AddTooPool(ANavMeshBoundsVolume* VolumeToAdd);

	int32 Score;
};
