// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "Mannequine.h"
#include "../Weapons/Gun.h"


// Sets default values
AMannequine::AMannequine()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FPCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	FP_Body = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPBody"));
	FP_Body->SetOnlyOwnerSee(true);
	FP_Body->SetupAttachment(FirstPersonCameraComponent);
	FP_Body->bCastDynamicShadow = false;
	FP_Body->CastShadow = false;
	FP_Body->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);;
	FP_Body->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
}

// Called when the game starts or when spawned
void AMannequine::BeginPlay()
{
	Super::BeginPlay();

	if (GunBlueprint == NULL) {

		UE_LOG(LogTemp, Warning, TEXT("Gun blueprint missing."));

		return;

	}

	GunActor = GetWorld()->SpawnActor<AGun>(GunBlueprint);

	if (IsPlayerControlled())
	{
		GunActor->AttachToComponent(FP_Body, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
		
	}
	else
	{
		GunActor->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
		
	}
	GunActor->TPAnimInstance = GetMesh()->GetAnimInstance();
	GunActor->FPAnimInstance = FP_Body->GetAnimInstance();

	if (InputComponent != NULL)
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AMannequine::PullTrigger);
	}
}

// Called every frame
void AMannequine::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMannequine::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	
}

void AMannequine::UnPossessed()
{
	Super::UnPossessed();

	if (GunActor == nullptr)
	{
		return;
	}

	GunActor->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
}

void AMannequine::PullTrigger()
{
	GunActor->OnFire();
}

