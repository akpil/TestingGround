// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "ChooseNextWayPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "PatrollRoots.h"

EBTNodeResult::Type UChooseNextWayPoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	// get Patrol points
	APawn* controlledPawn = OwnerComp.GetAIOwner()->GetPawn();
	UPatrollRoots* patrolRoute = controlledPawn->FindComponentByClass<UPatrollRoots>();

	if (!ensure(patrolRoute)) { return EBTNodeResult::Failed; }

	TArray<AActor*> patrolPoints = patrolRoute->GetPatrolPoints();

	if (patrolPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI guard is missing patrol points"));
		return EBTNodeResult::Failed;
	}

	// set next waypoint
	UBlackboardComponent* blackBoardComponent = OwnerComp.GetBlackboardComponent();
	int index = blackBoardComponent->GetValueAsInt(IndexKey.SelectedKeyName);
	blackBoardComponent->SetValueAsObject(NextWaypointKey.SelectedKeyName, patrolPoints[index]);

	// cycling index
	index++;
	index %= patrolPoints.Num();
	blackBoardComponent->SetValueAsInt(IndexKey.SelectedKeyName, index);

	return EBTNodeResult::Succeeded;
}
