// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "InfiniteTerrainGameMode.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "EngineUtils.h"
#include "ActorPool.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode()
{
	NaveMeshVolumePool = CreateDefaultSubobject<UActorPool>(FName("Nav Mesh Bounce Volume Pool"));
}

void AInfiniteTerrainGameMode::PopulateBoundsVolumePool()
{
	TActorIterator<ANavMeshBoundsVolume> VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld());
	while (VolumeIterator) // if it another actor exists
	{
		AddTooPool( *VolumeIterator);
		++VolumeIterator;
	}
}

void AInfiniteTerrainGameMode::NewTileConquered()
{
	Score++;
}

void AInfiniteTerrainGameMode::AddTooPool(ANavMeshBoundsVolume* VolumeToAdd)
{
	NaveMeshVolumePool->Add(VolumeToAdd);
}

